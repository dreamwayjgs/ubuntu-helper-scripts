#!/bin/sh
# Find the amount by doign
# synclient | grep HorizScrollDelta
# synclient | grep VertScrollDelta
# And then simply negative that value for this
# Found at 
# https://askubuntu.com/questions/690512/how-to-enable-natural-scrolling-in-xfce4/853262
# This is because, although xubuntu 18.04 has reverse scrolling option in touchpad
# settings, it doesn't really work, the setting does not cascade to gnome apps for some reason.
synclient HorizScrollDelta=-116
synclient VertScrollDelta=-116
